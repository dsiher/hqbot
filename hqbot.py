#Image Recognition
from PIL import Image
import pytesseract
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
import argparse
import cv2
import os
import random
import string
import tkinter as tk
import pyscreenshot as ImageGrab

def image_work():
	# Take screenshot
	root = tk.Tk()
	screen_width = root.winfo_screenwidth()
	screen_height = root.winfo_screenheight()
	name = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=16))
	im = ImageGrab.grab(bbox=(screen_width/2, screen_height/6, screen_width, screen_height*11/16))
	im.save(name + '.png')

	# Process Screenshot
	image = cv2.imread(name + ".png")
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.threshold(gray, 0, 255,
		cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
	gray = cv2.resize(gray, (0,0), fx=2, fy=1)
	gray = cv2.medianBlur(gray, 5)
	filename = "temp.png"
	cv2.imwrite(filename, gray)
	text = pytesseract.image_to_string(Image.open(filename), config='-c tessedit_char_whitelist=QqWwEeRrTtYyUuIiOoPpAaSsDdFfGgHhJjKkLlZzXxCcVvBbNnMm?`1234567890')
	os.remove(filename)
	clean_text = text.split("\n")
	clean_text = list(filter(None, clean_text))
	print(clean_text)
	return(clean_text)

#Answer Search
from bs4 import BeautifulSoup
from googlesearch import search
import operator
import re
import requests
import time
import urllib.parse
import urllib.request
from random import choice

def balthasar(question, choices):
	#Simple google search and search every page for the word
	match1 = 0
	match2 = 0
	match3 = 0
	urls = list(search(question))[:3]
	for url in urls:
		req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; BTRS86335; GTB7.5; (R1 1.6); .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; InfoPath.3; .NET CLR 2.0.50727)'})
		with urllib.request.urlopen(req) as response:
			page = response.read().decode('utf-8')
			match1 += sum(1 for match in re.finditer(choices[0], page))
			match2 += sum(1 for match in re.finditer(choices[1], page))
			match3 += sum(1 for match in re.finditer(choices[2], page))
	d = dict(zip(choices, [match1, match2, match3]))
	answer = str(max(d.items(), key=operator.itemgetter(1))[0])
	certainty = max([match1, match2, match3])/sum([match1, match2, match3])*100
	return {answer: certainty}


def caspar(question, choices):
	#googles the question with answer choices
	res = list()
	for i in choices:
		query = question + " " + i
		req = requests.get('http://www.google.com/search', params={'q': query})
		soup = BeautifulSoup(req.text, 'lxml')
		response = soup.find('div', {'id' : 'resultStats'})
		res_number = int("".join([s for s in list(response.text) if s.isdigit()]))
		res.append(res_number)
	d = dict(zip(choices, res))
	answer = str(max(d.items(), key=operator.itemgetter(1))[0])
	certainty = str(max(res)/sum(res)*100)[:5]
	return {answer: certainty}


def melchior(question, choices):
	#count mentions on the google search page
	match1 = 0
	match2 = 0
	match3 = 0
	url = "http://www.google.com/search?q=%s" % urllib.parse.quote(question)
	req = urllib.request.Request(url, headers={'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"})
	with urllib.request.urlopen(req) as response:
		page = response.read().decode('utf-8')
		match1 += sum(1 for match in re.finditer(choices[0], page))
		match2 += sum(1 for match in re.finditer(choices[1], page))
		match3 += sum(1 for match in re.finditer(choices[2], page))
	d = dict(zip(choices, [match1, match2, match3]))
	answer = str(max(d.items(), key=operator.itemgetter(1))[0])
	try:
	    certainty = max([match1, match2, match3])/sum([match1, match2, match3])*100
	    return {answer: certainty}
	except ArithmeticError:
	    return {answer: 0}


def three_magi(processed):
	q = -1
	question = ''
	choices = []
	for line in processed:
		if "?" in line:
			q = processed.index(line)
	for x in range(q+1):
		question += processed[x] + ' '
	for x in range(q+1, q+4):
		choices.append(processed[x])
	# print (balthasar(question, choices))
	print (caspar(question, choices))
	# print (melchior(question, choices))


if __name__ == '__main__':
	start_time = time.time()
	three_magi(image_work())
	print("--- %s seconds ---" % (time.time() - start_time))
